<?php

namespace App\Http\Controllers\Api;

use App\Facades\Covid;

class Covid19ApiController
{
    public $filename;

    public function index($date = null)
    {
        $filename = $this->setDate($date);
        try {
            $data = Covid::setDate($filename)->get();
            return response()->json($data, 200);
        } catch (\Exception $exception) {
            return response()->json(['error' => true, 'message' => $exception->getMessage()], 404);
        }
    }

    public function listCountry()
    {
        return response()->json(Covid::get()->pluck('country'), 200);
    }

    public function byCountry($country, $date = null)
    {
        $filename = $this->setDate($date);
        try {
            $data = Covid::setDate($filename)->country($country)->get();
            $data->last_update = $data->last_update->timezone('Asia/Jakarta')->format('Y-m-d'.'\T'.'H:i:s'."+07:00");
            return response()->json($data, 200);
        } catch (\Exception $exception) {
            return response()->json(['error' => true, 'message' => $exception->getMessage()], 404);
        }
    }

    private function setDate($date)
    {
        if (is_null($date)) {
            $filename = now()->format('Y-m-d');
        } else {
            $filename = $date;
        }

        return $filename;
    }
}
