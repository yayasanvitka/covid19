<?php

namespace App\Http\Controllers\Api;

use App\Facades\Covid;
use Illuminate\Support\Facades\Storage;

class BatamCovidController
{
    public function index($date = null)
    {
        if (is_null($date)) {
            $filename = now()->format('Y-m-d')."_batam.json";
        } else {
            $filename = $date."_batam.json";
        }

        if (Storage::exists($filename)) {
            return response()->json($this->readFile($filename), 200);
        } else {
            return response()->json(['error' => true, 'message' => 'Data Not Found'], 404);
        }
    }

    private function readFile($filename)
    {
        if (!Storage::disk('local')->has($filename)) {
            return $this->fetch();
        } else {
            return collect(json_decode(Storage::get($filename), true));
        }
    }
}
