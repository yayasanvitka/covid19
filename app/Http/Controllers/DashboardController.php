<?php

namespace App\Http\Controllers;

use App\Facades\Batam;
use App\Facades\Covid;

class DashboardController extends Controller
{
    public function index()
    {
        $yesterday = now()->yesterday()->format('Y-m-d');

        $data['indonesia'] = [
            'today' => collect(Covid::country('indonesia')->get()),
            'yesterday' => collect(Covid::setDate($yesterday)->country('indonesia')->get())
        ];
        $data['batam'] = [
            'today' => Batam::get()
        ];

        return view('dashboard.index', $data);
    }
}
