<?php

namespace App\Services\Covid;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Collection;

class GetBatamCity extends GetData
{
    protected $myCityData;

    public function fetchBatam()
    {
        $client = new Client();
        $last_update = now()->timezone('Asia/Jakarta')->timestamp;

        try {
            $result = $client->request("GET", "https://lawancorona.batam.go.id/peta-penyebaran/", []);
        } catch (ClientException $exception) {
            return ['error' => true, 'message' => $exception->getMessage()];
        }

        if ($result->getStatusCode() == 200) {
            $data = $result->getBody()->getContents();
            try {
                $data1 = explode('<div class="textwidget custom-html-widget">', $data);
                $data2 = explode('</div></div><div id="media_video-5"', $data1[1]);
                $mainData = explode("\r\n", $data2[0]);

                $this->myCityData = collect();

                foreach ($mainData as $key => $val) {
                    if (!$val == "") {

                        $valArr = explode("</td><td>", $val);
                        if (count($valArr) > 1) {
                            foreach ($valArr as $item) {
                                $this->pushToCollection($item);
                            }
                        } else {
                            $this->pushToCollection($val);
                        }
                    }
                }
                $this->myCityData = $this->formatMyCityData($this->myCityData);

                $filename = now()->format('Y-m-d')."_batam.json";
                $this->store(collect($this->myCityData), $filename);
            } catch (\Exception $exception) {
                return ['error' => true, 'message' => $exception->getMessage()];
            }
        }
    }

    private function pushToCollection($string)
    {
        $string = $this->removeHtmlTag(str_replace('  ', '', $string));
        if ($string == (string)"0") {
            $this->myCityData->add($string);
        } elseif (!is_null($string) && !empty($string)) {
            $this->myCityData->add($string);
        }
    }

    private function removeHtmlTag(string $string)
    {
        return strip_tags(trim($string));
    }

    private function formatMyCityData(Collection $collection)
    {
        $arr = [
            'tanggal' => $this->formatDate($collection[1]),
            'positif' => explode(" : ", $collection[2])[1],
            'odp' => [
                'total' => $collection[5],
                'sample' => $collection[7],
                'negatif' => $collection[9],
            ],
            'pdp' => [
                'total' => $collection[12],
                'dalam_pengawasan' => $collection[16],
                'sample' => $collection[14],
                'negatif' => $collection[18],
                'positif' => $collection[20],
            ]
        ];

        return $arr;
    }

    private function formatDate(string $stringTanggal): string
    {
        $arr = explode(" ", $stringTanggal);
        switch ($arr[1]) {
            case "Januari":
                $arr[1] = 1;
                break;
            case "Februari":
                $arr[1] = 2;
                break;
            case "Maret":
                $arr[1] = 3;
                break;
            case "April":
                $arr[1] = 4;
                break;
            case "Mei":
                $arr[1] = 5;
                break;
            case "Juni":
                $arr[1] = 6;
                break;
            case "Juli":
                $arr[1] = 7;
                break;
            case "Agustus":
                $arr[1] = 8;
                break;
            case "September":
                $arr[1] = 9;
                break;
            case "Oktober":
                $arr[1] = 10;
                break;
            case "November":
                $arr[1] = 11;
                break;
            case "Desember":
                $arr[1] = 12;
                break;
        }

        $arr[1] = str_pad($arr[1], 2, "0", STR_PAD_LEFT);
        krsort($arr);
        return implode("-", $arr);
    }
}
