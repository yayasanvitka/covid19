<?php

namespace App\Services\Covid;

use App\Traits\ReadFileTrait;
use Carbon\Carbon;

class Covid
{
    use ReadFileTrait;

    public $controller;
    public $total;

    public function __construct()
    {
        $this->filename = now()->format('Y-m-d') . "_global.json";
        $this->initData();
    }

    private function initData()
    {
        $data = collect($this->readFile());

        $this->data = collect();

        foreach ($data as $value) {
            $rData = (object) $value;
            $rData->last_update = Carbon::createFromTimestampUTC(substr($value['last_update'], 0, 10));
            $this->data = $this->data->push($rData);
        }

        $this->total();

        return $this;
    }

    public function setDate($date)
    {
        $this->filename = $date . "_global.json";
        $this->initData();
        return $this;
    }

    public function total()
    {
        $this->total = new Total($this->data);
        return $this->total;
    }

    public function fetch()
    {
        $getdata = new GetData();
        return $getdata->fetch();
    }

    public function country($countryName)
    {
        $this->data = $this->data->filter(function ($value, $key) use ($countryName) {
            if (strtolower($value->country) == strtolower($countryName)) {
                return $value;
            }
        });

        return $this;
    }

}
