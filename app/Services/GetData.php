<?php

namespace App\Services\Covid;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class GetData
{
    protected $filename;

    public function fetch()
    {
        $client = new Client();
        $last_update = now()->timezone('Asia/Jakarta')->timestamp;

        try {
            $result = $client->request("GET", config('covid.endpoint'), [
                'headers' => [
                    "Accept" => "*/*",
                    "Content-Type" => "application/json"
                ]
            ]);
        } catch (ClientException $exception) {
            return ['error' => true, 'message' => $exception->getMessage()];
        }

        if ($result->getStatusCode() == 200) {
            $data = collect(json_decode($result->getBody(), true));
            try {
                foreach ($data as $key=>$val) {
                    $val['last_update'] = $last_update;
                    $data[$key] = $val;
                }

                $filename = now()->format('Y-m-d') . "_global.json";
                $this->store($data, $filename);

            } catch (\Exception $exception) {
                throw new \Exception($exception->getMessage(), $exception->getCode());
            }
        }

        if (config('covid.load_batam')) {
            $myCity = new GetBatamCity();
            try {
                $myCity->fetchBatam();
            } catch (\Exception $exception) {
                throw new \Exception($exception->getMessage(), $exception->getCode());
            }
        }

        return $data;
    }

    protected function store(Collection $data, $filename)
    {
        Storage::disk('local')->put($filename, $data->toJson());
        return true;
    }
}
