<?php

namespace App\Services\Covid;

use App\Traits\ReadFileTrait;
use Carbon\Carbon;

class Batam
{
    use ReadFileTrait;

    public function __construct()
    {
        $this->filename = now()->format('Y-m-d') . "_batam.json";
        $this->initData();
    }

    private function initData()
    {
        $this->data = collect($this->readFile());

        return $this;
    }

    public function setDate($date)
    {
        $this->filename = $date . "_batam.json";
        $this->initData();
        return $this;
    }
}
