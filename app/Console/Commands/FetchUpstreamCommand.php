<?php

namespace App\Console\Commands;

use App\Services\Covid\GetData;
use Illuminate\Console\Command;
use App\Facades\Covid;

class FetchUpstreamCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'covid19-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get data from upstream.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Getting new data..");
        try{
            $getData = new GetData();
            $getData->fetch();
            $this->info("Done!");
        } catch (\Exception $exception) {
            $this->error("Error: " . $exception->getCode());
            $this->error($exception->getMessage());
        }
    }
}
