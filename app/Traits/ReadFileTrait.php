<?php

namespace App\Traits;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

trait ReadFileTrait
{
    protected $filename;
    public $data;

    /**
     * @return mixed
     */
    public function get()
    {
        if ($this->data->count() == 1) {
            return $this->data->first();
        }
        return $this->data;
    }

    /**
     * @return Collection
     * @throws FileNotFoundException
     */
    private function readFile()
    {
        if (!Storage::disk('local')->has($this->filename)) {
            throw new \Exception("Data Not Found");
        } else {
            return collect(json_decode(Storage::disk('local')->get($this->filename), true));
        }
    }
}
