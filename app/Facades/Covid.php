<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Covid extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \App\Services\Covid\Covid::class;
    }
}
