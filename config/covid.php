<?php
return [
    'endpoint' => env('COVID_ENDPOINT', "https://coronavirus-19-api.herokuapp.com/countries/"),
    'load_batam' => env('COVID_LOAD_CITY_BATAM', false),
];
