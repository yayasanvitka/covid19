<div class="col-sm-6 col-lg-3">
    <div class="card text-white bg-gradient-danger">
        <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
            <div>
                <div class="text-value-lg">{{ $value }}</div>
                <div>
                    {!! $content !!}
                </div>
            </div>
        </div>
        @include("dashboard._card-footer")
    </div>
</div>
