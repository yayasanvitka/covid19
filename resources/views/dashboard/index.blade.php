<!doctype html>
<html lang="id">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CoreUI CSS -->
    <link rel="stylesheet" href="https://unpkg.com/@coreui/coreui@3.0.0-rc.0/dist/css/coreui.min.css"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/@coreui/icons/css/free.min.css">

    <title>Covid-19 Dashboard</title>
</head>
<body class="c-app">
<div class="c-wrapper">

    <div class="c-body">
        <main class="c-main">
            <div class="container">
                <div class="row">
                    <h2>Kasus di Indonesia Saat Ini</h2>
                </div>
                <div class="row">

                    @php($perawatan = $indonesia['today']['active'] - $indonesia['yesterday']['active'])

                    @if ($perawatan > 0)
                        @php($perawatan_arrow_class = '<i class="cil-arrow-thick-top"></i>')
                    @else
                        @php($perawatan_arrow_class = '<i class="cil-arrow-thick-bottom"></i>')
                    @endif

                    @include("dashboard._card-info", [
                        'value' => $indonesia['today']['cases'],
                        'content' => 'Terkonfirmasi (Hari ini: <i class="cil-arrow-thick-top"></i><strong>'. $indonesia['today']['todayCases'] .'</strong>)'])

                    @include("dashboard._card-warning", [
                        'value' => $indonesia['today']['active'],
                        'content' => 'Dalam Perawatan (Hari ini: '. $perawatan_arrow_class . $perawatan .'</strong>)'])

                    @include("dashboard._card-success", [
                        'value' => $indonesia['today']['recovered'],
                        'content' => 'Sembuh'])

                    @include("dashboard._card-danger", [
                        'value' => $indonesia['today']['deaths'],
                        'content' => 'Meninggal (Hari ini: <i class="cil-arrow-thick-top"></i><strong>'. $indonesia['today']['todayDeaths'] .'</strong>)'])
                </div>
                @include("dashboard._lastUpdate", ['value' => $indonesia['today']['last_update']])
                <div class="row" style="margin-top: 20px">
                    <h2>Kasus di Batam Saat Ini</h2>
                </div>
                <div class="row">
                    <h4>Orang Dalam Pemantauan (ODP)</h4>
                </div>
                <div class="row">
                    @include("dashboard._card-info", [
                        'value' => $batam['today']['odp']['total'],
                        'content' => 'Jumlah ODP'])

                    @include("dashboard._card-warning", [
                        'value' => $batam['today']['odp']['sample'],
                        'content' => 'Dalam Pemantauan'])

                    @include("dashboard._card-success", [
                        'value' => $batam['today']['odp']['negatif'],
                        'content' => 'Selesai Pemantauan'])
                </div>
                <div class="row">
                    <h4>Pasien Dalam Pengawasan (PDP)</h4>
                </div>
                <div class="row">
                    @include("dashboard._card-info", [
                        'value' => $batam['today']['pdp']['total'],
                        'content' => 'Jumlah PDP'])

                    @include("dashboard._card-warning", [
                        'value' => $batam['today']['pdp']['dalam_pengawasan'],
                        'content' => 'Dalam Proses Pengawasan'])

                    @include("dashboard._card-success", [
                        'value' => $batam['today']['pdp']['negatif'],
                        'content' => 'Negatif'])

                    @include("dashboard._card-danger", [
                        'value' => $batam['today']['pdp']['positif'],
                        'content' => 'Positif'])
                </div>
                @include("dashboard._lastUpdate", ['value' => $batam['today']['tanggal']])
            </div>

        </main>
    </div>
    <footer class="c-footer">
        Covid-19 Yayasan Vitka - Data Source
        <a href="https://coronavirus-19-api.herokuapp.com/countries/" target="_blank"> &nbsp;Global&nbsp; </a>&
        <a href="http://lawancorona.batam.go.id/" target="_blank">&nbsp;Batam</a>.
        &nbsp;&nbsp;|&nbsp;&nbsp;<a href="{{ url('/api/global') }}">API</a>
    </footer>
</div>
</body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.15.0/umd/popper.min.js"
        integrity="sha384-L2pyEeut/H3mtgCBaUNw7KWzp5n9&#43;4pDQiExs933/5QfaTh8YStYFFkOzSoXjlTb"
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/@coreui/coreui@3.0.0-rc.0/dist/js/coreui.min.js"></script>
</body>
</html>

