<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'api', 'namespace' => 'Api'], function () use ($router) {
    $router->get('global', 'Covid19ApiController@index');
    $router->get('global/{date}', 'Covid19ApiController@index');

    $router->get('city/batam', 'BatamCovidController@index');

    $router->get('city/batam/{date}', 'BatamCovidController@index');

    $router->group(['prefix' => 'countries'], function () use ($router) {

        $router->get('/', 'Covid19ApiController@listCountry');

        $router->get('{country}', 'Covid19ApiController@byCountry');

        $router->get('{country}/{date}', 'Covid19ApiController@byCountry');
    });

});
