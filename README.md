# COVID-19 API
[![Latest Stable Version](https://img.shields.io/packagist/v/yayasanvitka/covid19.svg)](https://packagist.org/packages/yayasanvitka/covid19)
[![Total Downloads](https://img.shields.io/packagist/dt/yayasanvitka/covid19.svg)](https://packagist.org/packages/yayasanvitka/covid19)
[![License](https://img.shields.io/packagist/l/yayasanvitka/covid19.svg)](https://packagist.org/packages/yayasanvitka/covid19)

## Introduction
`yayasanvitka/covid19` is a [Lumen](https://lumen.laravel.com/docs/7.x/) based application which was created to easily publish and consume the API for live information about Novel Coronavirus-19.
This module is based on API from [javieraviles/covidAPI](https://github.com/javieraviles/covidAPI). 

## Documentation
### Requirement
This application is created with Lumen Framework, and required the same package as [Lumen 7](https://lumen.laravel.com/docs/7.x/installation#server-requirements).

### Installation & Setup
<code>composer create-project --prefer-dist yayasanvitka/covid19</code>

If you want to set config, for example disable Batam City, Indonesia data loading, you can set `COVID_LOAD_CITY_BATAM` on your .env to false. 

### Web Route
<ul>
<li>/api/global: Global Data</li>
<li>/api/global/{DATE}: Global Data by date</li>
<li>/api/countries: List of countries</li>
<li>/api/countries/{COUNTRY}: Data per Country</li>
<li>/api/countries/{COUNTRY}/{DATE}: Data per Country by date</li>
<li>/api/city/batam: Data for Batam, Indonesia</li>
<li>/api/city/batam/{DATE}: Data for Batam, Indonesia by date</li>
</ul>

### Facade Methods

- <code>Covid::fetch()</code>
<br />Fetch data from endpoint
- <code>Covid::get()</code>
<br />Get The Data. Returns *\Illuminate\Support\Collection*. Can be chained with *country()* method.
- <code>Covid::country()</code>
<br />Get the country data.
- <code>Covid::total()->cases</code>
<br /> Get total data (Global) for cases 
- <code>Covid::total()->todayCases</code>
<br /> Get total data (Global) for todayCases 
- <code>Covid::total()->todayDeath</code>
<br /> Get total data (Global) for todayDeath 
- <code>Covid::total()->recovered</code>
<br /> Get total data (Global) for recovered 
- <code>Covid::total()->deaths</code>
<br /> Get total data (Global) for deaths 
- <code>Covid::total()->active</code>
<br /> Get total data (Global) for active 
- <code>Covid::total()->critical</code>
<br /> Get total data (Global) for critical 
- <code>Covid::total()->casesPerOneMillion</code>
<br /> Get total data (Global) for casesPerOneMillion 

### Artisan Command
<code>php artisan covid19-update</code>
<br />Run this to update the database

## License
MIT License 2020, IT Yayasan Vitka.
Based on API from [javieraviles](https://github.com/javieraviles/covidAPI), the data may not be used for commercial purposes.
Feel free to exted the application for your city, and please contact us on [adly@yayasanvitka.id](mailto:adly@yayasanvitka.id) if you found any bugs.
